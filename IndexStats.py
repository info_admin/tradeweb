import pandas as pd
from DataHandler import DataHandler
from datetime import datetime
#import helper_2 as help

import numpy as np
from scipy.stats import linregress

class IndexStats(object):

    def __init__(self,DhObj_stats,IndexObj,Attributes,BenchmarkStr):

        self.indexobj = IndexObj
        self.datahandler = DhObj_stats
        self.benchmark = BenchmarkStr

        self.get_stats()
        self.get_sector_dist()

    def get_sector_dist(self):
        
        #get sector distribution
        CUSIP_INSTERSECTION_ = list(set(self.indexobj.index_result['IndexCon']).intersection(self.datahandler.ticker_data_mean.index.tolist()))
        df1 = self.datahandler.ticker_data_mean.loc[CUSIP_INSTERSECTION_]
        df1 = df1['SECTOR'].value_counts()
        df2 = df1/df1.sum()
        df_print = pd.merge(df1.to_frame(),df2.to_frame(),left_index=True,right_index=True)
        df_print.rename(columns={'SECTOR_x':'Count','SECTOR_y':'Concentration'},inplace=True)

        self.df_sectors_dist = df_print

        return

    def get_stats(self, QUARTERLY = False):

        df_price_data = DataHandler.dict_to_df(self.datahandler.ticker_data,'MID')
        INDEX_LIST = list(set(self.indexobj.index_result['IndexCon']).intersection(df_price_data.index.tolist()))
        INDEX_WEIGHT = 1 / len(INDEX_LIST) #equal
        
        BENCH_WEIGHT = DataHandler.get_benchmark_data(self.datahandler.segment_)
        BENCH_WEIGHT = BENCH_WEIGHT[BENCH_WEIGHT.index.isin(df_price_data.index)]
        BENCH_LIST = BENCH_WEIGHT.index.tolist()

        #run total return
        df_index=IndexStats.calculate_TR(df_price_data,self.datahandler.termsheet['CPN'],1000.0,cusip_list=INDEX_LIST,weight=None)
        df_bench=IndexStats.calculate_TR(df_price_data,self.datahandler.termsheet['CPN'],1000.0,cusip_list=BENCH_LIST,weight=BENCH_WEIGHT)

        #get beta
        beta_ = IndexStats.stats(df_index.sum(),df_bench.sum())
        df_stats = pd.DataFrame(index = ['Beta','DWAS','VWAS','VWAP','Total DV01'],columns = ['CustomIndex','BenchmarkIndex'])
        df_stats.loc['Beta']['CustomIndex'] = beta_
        df_stats.loc['Beta']['BenchmarkIndex'] = 1.0

        #VWAP,VWAS,DWAS
        df_tickervol_mid_sprd = DataHandler.dict_to_df(self.datahandler.ticker_data,['MID','TICKERVOL','FULLSPRD','DV01PERMM'])
        df_tickervol_mid_sprd=df_tickervol_mid_sprd.T.reset_index().set_index(['Date','Variable'])
        
        max_date= self.datahandler.best_data_date
        df_data_now= df_tickervol_mid_sprd.iloc[df_tickervol_mid_sprd.index.get_level_values('Date') == max_date].copy()
        df_data_now = df_data_now.T
        df_data_now.columns=df_data_now.columns.droplevel()

        #--------------------------BENCHMARK

        df_merge = pd.merge(df_data_now['MID'].to_frame(),BENCH_WEIGHT,left_index=True,right_index=True).dropna()
        df_stats.loc['VWAP']['BenchmarkIndex'] = np.average(df_merge['MID'],weights=df_merge['Weight'])

        df_merge = pd.merge(df_data_now['FULLSPRD'].to_frame(),BENCH_WEIGHT,left_index=True,right_index=True).dropna()
        df_stats.loc['VWAS']['BenchmarkIndex'] = np.average(df_merge['FULLSPRD'],weights=df_merge['Weight'])

        DVO1_trade = df_data_now.loc[BENCH_LIST]['DV01PERMM'].divide(1000).dropna()
        df_stats.loc['DWAS']['BenchmarkIndex'] = np.average(df_data_now.loc[DVO1_trade.index.tolist()]['FULLSPRD'],weights = DVO1_trade)
        df_stats.loc['Total DV01']['BenchmarkIndex'] =  df_data_now.loc[BENCH_LIST]['DV01PERMM'].sum()

        #--------------------------CUSTOM
        df_stats.loc['VWAP']['CustomIndex'] = df_data_now.loc[INDEX_LIST]['MID'].mean()
        df_stats.loc['VWAS']['CustomIndex'] = df_data_now.loc[INDEX_LIST]['FULLSPRD'].mean()

        DVO1_trade = df_data_now.loc[INDEX_LIST]['DV01PERMM'].divide(1000).dropna()
        df_stats.loc['DWAS']['CustomIndex'] =  np.average(df_data_now.loc[DVO1_trade.index.tolist()]['FULLSPRD'],weights = DVO1_trade)
        df_stats.loc['Total DV01']['CustomIndex'] =  df_data_now.loc[INDEX_LIST]['DV01PERMM'].sum()

        self.df_stats = df_stats

        return

    @staticmethod
    def stats(df_index,df_bench):
        
        #beta
        x = np.array(df_index)
        y = np.array(df_bench)
        beta_out = linregress(x,y)

        return beta_out.slope
    
    @staticmethod
    def calculate_TR(df_price,df_coupon,bond_notional,cusip_list=None,weight=None):
        '''
        Args:
            df_price: indexed to cusip, columns = date
            df_weights: indexed to cusip
            df_coupon: indexed to cusip
            if weight=None, assumes weighting is equal. If provided df_weight: indxed on cusip.
        '''

        if cusip_list==None:
            cusip_list = df_price.index.tolist()

        df_price=df_price.reindex(cusip_list)
        df_trading_day = df_price.copy()
        df_trading_day = df_trading_day.notnull()
        df_trading_day = df_trading_day.astype('int') #matrix of 1's and 0's to indicate days traded.

        df_coupon = df_coupon.reindex(cusip_list).astype('float') / 100.00 #make it percentage decimal
        df_coupon = (df_coupon*bond_notional)/365.0   #need to get trading days (get offical calendar)
        df_acc = df_trading_day.mul(df_coupon,axis='index').copy()
        df_return = df_price.add(df_acc,axis='index')

        if isinstance(weight,pd.DataFrame):
            df_return=df_return.mul(weight['Weight'],axis='index')
        else:
            df_return=df_return.mul(1.0/float(len(cusip_list)))

        return df_return


