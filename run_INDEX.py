from IndexGeneration import ThemeBasedIndex,ETFBasedIndex
from DataHandler import DataHandler
from IndexStats import IndexStats
from Constraints import PRINT_LIST_
import pandas as pd

#import helper_2 as help
#from my_app.editor_utils.utils import createDataGrid, addtoDataSet, createDataChart

WORKSPACE = False

def run_INDEX(LookBack,Attributes,IndexSize,MaxSectorConcentration,BetaOptimiser,ReplaceList):

    #create datahandler
    print('Gathering Fixed Income data..')
    DhObj = DataHandler(Attributes,LookBack)

    DhObj_stats = DataHandler(Attributes,100) #datahandler for stats

    replacement_iterations_list = [[]]

    for i in ReplaceList:
        replacement_iterations_list.append(i)

    objectdict = {}
    count = -1
    for Replacements in replacement_iterations_list:
        count=count + 1
        print('Generating custom index:' + str(count+1))
        if DhObj.segment_ == 'IG' or DhObj.segment_ == 'HY' or DhObj.segment_ == 'LATAM':
            
            #create theme index.
            indexobj = ThemeBasedIndex(DhObj,DhObj_stats,Attributes,LookBack,IndexSize,MaxSectorConcentration,BetaOptimiser,Replacements)  
            
        elif DhObj.segment_ == 'LQD.US' or DhObj.segment_ == 'HYG.US':
            
            #minic existing index.
            indexobj= ETFBasedIndex(DhObj,DhObj_stats,Attributes,LookBack,IndexSize,MaxSectorConcentration,BetaOptimiser,Replacements)
            
        else:
            raise ValueError('ALLINDEX User Error: Market segment not recognised. Please select either IG or HY.')

        
        statobj = IndexStats(DhObj_stats,indexobj,Attributes,DhObj.segment_)

        objectdict[count] = [statobj,indexobj]

    pd.set_option('display.max_rows', 500)
    pd.set_option('display.max_columns', 500)
    pd.set_option('display.width', 150)

    print_index(objectdict,DhObj)
    print_stats(objectdict)
    print_sectors(objectdict)

    return

def print_stats(objectdict):

    printdict = {}
    eval_str_1 = ''
    
    for key, value in objectdict.items():
        printdict[key] = value[0].df_stats
        eval_str_1 = eval_str_1 + 'printdict[' + str(key) + "]['CustomIndex'],"

    eval_str_1='pd.concat([' + eval_str_1[:-1] + '],axis=1)'
   
    df_print_stats = eval(eval_str_1)
    df_bench = printdict[key]['BenchmarkIndex']
    df_print_stats = pd.concat([df_print_stats,df_bench],axis = 1)

    index_list=[]
    for i in objectdict.keys():
        j = value[0].df_stats.columns.tolist()[0]
        index_list.append((i+1,j))

    index_list.append(('','BenchmarkIndex'))

    M_index = pd.MultiIndex.from_tuples(index_list)
    df_print_stats= pd.DataFrame(data=df_print_stats.values, columns=M_index, index=df_print_stats.index)
    print('------------INDEX STATS----------')
    print(df_print_stats)

    return

def print_sectors(objectdict):

    printdict = {}
    eval_str_1 = ''
    
    for key, value in objectdict.items():

        printdict[key] = value[0].df_sectors_dist
        eval_str_1 = eval_str_1 + 'printdict[' + str(key) + '],'

    eval_str_1='pd.concat([' + eval_str_1[:-1] + '],axis=1)'

    df_print_stats = eval(eval_str_1)

    index_list=[]
    for i in objectdict.keys():
        for j in value[0].df_sectors_dist.columns.tolist():
            index_list.append((i+1,j))

    M_index = pd.MultiIndex.from_tuples(index_list)
    df_print_stats= pd.DataFrame(data=df_print_stats.values, columns=M_index, index=df_print_stats.index)

    #df_print_stats['Concentration'] = df_print_stats['Concentration'].map(lambda x: '%2.1f' % x)
    
    print('------------SECTOR DISTRIBUTION----------')
    print(df_print_stats.fillna('-'))

    return
    
def print_index(objectdict,DhObj):

    printdict = {}
    eval_str_1 = ''
    eval_str_2 = eval_str_1

    for key, value in objectdict.items():

        df1,df2 = get_index_data(value[1],DhObj)

        printdict[key] = [df1,df2]

        eval_str_1 = eval_str_1 + 'printdict[' + str(key) + '][0],'
        eval_str_2 = eval_str_2 + 'printdict[' + str(key) + '][1],'

    eval_str_1='pd.concat([' + eval_str_1[:-1] + '],axis=1)'
    eval_str_2='pd.concat([' + eval_str_2[:-1] + '],axis=1)'

    df_print_1 = eval(eval_str_1)
    df_print_2 = eval(eval_str_2)

    index_list=[]
    for i in objectdict.keys():
        for j in df1.columns.tolist():
            index_list.append((i+1,j))

    
    M_index = pd.MultiIndex.from_tuples(index_list)
    df_print_1 = pd.DataFrame(data=df_print_1.values, columns=M_index, index=df_print_1.index)
    print('---------------------- CUSTOM BOND INDEX ----------------------')
    print(df_print_1)
    
    if WORKSPACE:
        values_ = df_print_1.T.values.tolist()
        headers_ = [[i[1] + '_' + str(i[0])  for i in df_print_1.columns.tolist()]]
        help.gridHelp_1([values_],headers_)
    
    try:
        df_print_2 = pd.DataFrame(data=df_print_2.values, columns=M_index, index=df_print_2.index)
        print('------------------------ ALTERNATIVE BONDS -----------------------')
        print(df_print_2)

    except:
        pass
    
    return

def get_index_data(indexobj,datahandler):
    
    PRINT_ATTRIBUTES_ = list(set(PRINT_LIST_ + list(indexobj.attributes.keys())))

    df1 = datahandler.ticker_data_mean.loc[indexobj.index_result['IndexCon']]
    df1 = df1[PRINT_ATTRIBUTES_]

    if isinstance(indexobj.df_beta,pd.DataFrame):
        df1 = pd.merge(df1,indexobj.df_beta,left_index=True,right_index=True)

    if indexobj.index_result['IndexSug']:
        df2 = datahandler.ticker_data_mean.loc[indexobj.index_result['IndexSug']]
        df2 = df2[PRINT_ATTRIBUTES_]
        if isinstance(indexobj.df_beta,pd.DataFrame):
            df2 = pd.merge(df2,indexobj.df_beta,left_index=True,right_index=True)
    else:
        df2 = pd.DataFrame()
    return df1.reset_index().rename(columns={'index': 'CUSIP'}),df2.reset_index().rename(columns={'index': 'CUSIP'})

