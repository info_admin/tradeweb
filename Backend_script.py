"""
@author: br@allindex.com (Quant. Dev) - Benjamin Robinson

TradeWeb and ALLINDEX POC. 

Inputs:
    LookBack (int): Select hows many days to average over looking backwards. For 
    example, if selected as 5 days the average of the most recent 5 trading 
    days will be used to calculate the index.
    
    IndexSize (int): Number of bonds in the custom index.
    
    MaxSectorConcentration (float between 0 and 1): Maximum conncentation of an individual sector as a percentage decimal.
    
    Attributes (dict): Contains a dictionary where the key represents the attribute name for the index
    and the items are a list containing the values.
    
    There are two types of attributes which can be input into the attributes dictionary:
    
    FIXED ATTRIBUTES insist that the index only contains bonds with the provided attribute value. For example, if a 'MAT' (maturity) 
    attribute is set to a value of 5, only 5 year bonds will be included in the index. Therefore the number of bonds in the index will be limited
    to the number of 5 year bonds available. 
    
    FIXED ATTRIBUTES = ['SECTOR','CURR','MAT','MKTSEG','BENCHMAT'] 
    
    How to fill the attributes_dictionary (for FIXED ATTRIBUTES):
    
                 Attribute_dictionary = {
                 
                    'attribute': [variables],
                    
                    'SECTOR': ['+Yankees','+UtilsIG'],
                    'MAT': [10,20],
                }
                
                Note: for SECTOR + or - must be added to include or exclude sectors.
    
    
    NON-FIXED ATTRIBUTES are ranked and weighted. Meaning bonds with the nearest attribute value will be selected. For example, 
    if 'LIQUIDITYSCORE' is set to 10, and no bonds have a liquidity score of 10 then then bonds will the closest liquidityscore to 10 
    will be returned.
    
    NON-FIXED ATTRIBUTES = ['LIQUIDITYSCORE','BID','ASK','MID','AVWERROR','AVWIDLTA','CNFD','EMAVOL','ESTCONFCLS','ESTPRICE',
                                'ESTPRICECHG', 'ESTPRICECLS', 'FULLSPRD','ISPDCHG','ISPDCLS','SELFVOL','TICKERVOL',
                                'TSPD','TSPDCLS','VOLM','VOLMALL','VOLMDAYS','DV01PERMM','CPN','CURR']
    
    How to fill the attributes_dictionary (for NON-FIXED ATTRIBUTES):
    
                Attribute_dictionary = {

                    'attribute': [weight, indicator],

                    'LIQUIDITYSCORE': [0.2, 'High'],
                    'ESTPRICE': [0.2,95],
                    'FULLSPRD':[0.6,'High'] 
                }
                
                 weight (int) is coefficent between 0 and 1 which indicates the weight to be 
                applied to this attribute. 
                
                indicator (str/float) is used to determine select whether a high or low 
                rank is desried for the attribute. If a float is passed a target value be selected.
 """

from run_INDEX import run_INDEX
import pandas as pd


try:
    inputs = json.loads(fe_inputs)
except:
    print("No input.Using defaults...")
    inputs = {
    "IndexSize":"10",
    "LookBack":"5",
    "MaxSectorConcentration":"",
    "Rating":">AAA",
    "Sectors":["GenTMTInd","Yankees"],
    "IncludeExclude":False,
    "MAT":  ['7'],
    "MKTSEG":["LQD.US"],
    "BENCHMAT": ['7'],
    "ExcludeBonds":"",
    "PARAMS":
        [
            {
            "type":"LIQUIDITYSCORE",
            "weight":0.2,
            "low_high":"High",
            "target":None
            }
        ],
    "BetaOptimiser":False

}

LookBack = int(inputs.get('LookBack'))
IndexSize = int(inputs.get('IndexSize'))

if len(inputs.get('MaxSectorConcentration')) == 0:
    MaxSectorConcentration=None 
else:
    MaxSectorConcentration=float(inputs.get('MaxSectorConcentration'))
    
BetaOptimiser=inputs.get('BetaOptimiser')
ReplaceList=inputs.get('ExcludeBonds') #can be nested for multiple indices.
if not ReplaceList:
    ReplaceList = []
else: 
    ReplaceList = eval(str(ReplaceList))

Attributes = {}

"""FIXED ATTRS"""
Sectors = inputs.get("Sectors")
IncludeExclude = inputs.get("IncludeExclude")

if Sectors:
    Attributes['SECTOR']= []
    for Sector in Sectors:
        Attributes['SECTOR'].append(("-" if IncludeExclude else "+") + Sector)

#if inputs.get('CURR'):Attributes['CURR'] = inputs.get('CURR') if type(inputs.get('CURR'))==list else [inputs.get('CURR')]
if inputs.get('MAT') != 'any': 
    if inputs.get('MAT'):Attributes['MAT'] = inputs.get('MAT') if type(inputs.get('MAT'))==list else [inputs.get('MAT')]

if inputs.get('MKTSEG'):Attributes['MKTSEG'] = inputs.get('MKTSEG') if type(inputs.get('MKTSEG'))==list else [inputs.get('MKTSEG')]

if inputs.get('BENCHMAT') != 'any':
    if inputs.get('BENCHMAT'):Attributes['BENCHMAT'] = inputs.get('BENCHMAT') if type(inputs.get('BENCHMAT'))==list else [inputs.get('BENCHMAT')]

"""NON FIXED ATTRS"""
for param in inputs.get('PARAMS'):
    Attributes[param['type']]=[float(param['weight']),param['low_high'] if param['low_high']!='Target' else float(param['target'])]
print(Attributes)

print(LookBack)
print(IndexSize)
print(MaxSectorConcentration)
print(ReplaceList)
print(BetaOptimiser)

run_INDEX(LookBack,Attributes,IndexSize,MaxSectorConcentration,BetaOptimiser,ReplaceList)

