"""
@author: br@allindex.com (Quant. Dev) - Benjamin Robinson

TradeWeb and ALLINDEX POC. 

Inputs:
    LookBack (int): Select hows many days to average over looking backwards. For 
    example, if selected as 5 days the average of the most recent 5 trading 
    days will be used to calculate the index.
    
    IndexSize (int): Number of bonds in the custom index.
    
    MaxSectorConcentration (float between 0 and 1): Maximum conncentation of an individual sector as a percentage decimal.
    
    Attributes (dict): Contains a dictionary where the key represents the attribute name for the index
    and the items are a list containing the values.
    
    There are two types of attributes which can be input into the attributes dictionary:
    
    FIXED ATTRIBUTES insist that the index only contains bonds with the provided attribute value. For example, if a 'MAT' (maturity) 
    attribute is set to a value of 5, only 5 year bonds will be included in the index. Therefore the number of bonds in the index will be limited
    to the number of 5 year bonds available. 
    
    FIXED ATTRIBUTES = ['SECTOR','CURR','MAT','MKTSEG','BENCHMAT'] 
    
    How to fill the attributes_dictionary (for FIXED ATTRIBUTES):
    
                 Attribute_dictionary = {
                 
                    'attribute': [variables],
                    
                    'SECTOR': ['+Yankees','+UtilsIG'],
                    'MAT': [10,20],
                }
                
                Note: for SECTOR + or - must be added to include or exclude sectors.
    
    
    NON-FIXED ATTRIBUTES are ranked and weighted. Meaning bonds with the nearest attribute value will be selected. For example, 
    if 'LIQUIDITYSCORE' is set to 10, and no bonds have a liquidity score of 10 then then bonds will the closest liquidityscore to 10 
    will be returned.
    
    NON-FIXED ATTRIBUTES = ['BID','ASK','MID','AVWERROR','AVWIDLTA','CNFD','EMAVOL','ESTCONFCLS','ESTPRICE',
                                'ESTPRICECHG', 'ESTPRICECLS', 'FULLSPRD','ISPDCHG','ISPDCLS','SELFVOL','TICKERVOL',
                                'TSPD','TSPDCLS','VOLM','VOLMALL','VOLMDAYS','DV01PERMM','CPN','CURR']
    
    How to fill the attributes_dictionary (for NON-FIXED ATTRIBUTES):
    
                Attribute_dictionary = {

                    'attribute': [weight, indicator],

                    'LIQUIDITYSCORE': [0.2, 'High'],
                    'ESTPRICE': [0.2,95],
                    'FULLSPRD':[0.6,'High'] 
                }
                
                 weight (int) is coefficent between 0 and 1 which indicates the weight to be 
                applied to this attribute. 
                
                indicator (str/float) is used to determine select whether a high or low 
                rank is desried for the attribute. If a float is passed a target value be selected.
 """

from run_INDEX import run_INDEX
import pandas as pd

LookBack = 1
IndexSize = 30
MaxSectorConcentration=0.1
BetaOptimiser=True
ReplaceList=[] #can be nested for multiple indices.
Attributes={'SECTOR': ['+BankFin', '+TMT', '+UtilsIG', '+RailTrans', '+RetGroc', '+Yankees', '+Ins', '+EnergyIG', '+REITSIG', '+ConCycDisc'], 'MKTSEG': ['LQD.US'], 'LIQUIDITYSCORE': [100.0, 'High']}

run_INDEX(LookBack,Attributes,IndexSize,MaxSectorConcentration,BetaOptimiser,ReplaceList)


