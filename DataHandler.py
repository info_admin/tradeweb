# -*- coding: utf-8 -*-
"""
Created on Thu Dec 16 11:30:24 2021

@author: Benjamin Robinson (Quant. Dev) - br@allindex.com
"""
import pandas as pd
import numpy as np
import glob
import os
import re
from io import StringIO
import pysftp
import requests
import datetime
from dateutil.relativedelta import relativedelta
from Constraints import ATTRIBUTES_LIST_

class DataHandler(object):
    '''
    Base class datahandler.
    '''
    pass

class DataHandler(DataHandler):
    
    def __init__(self,attributes,look_back):
        
        self.attributes = attributes
        self.look_back = look_back
        
        self.get_data()
        
    def get_data(self):
        
        #get seg.
        if 'MKTSEG' in self.attributes.keys():
            file_segment_  = self.attributes['MKTSEG'][0]
        else:
            file_segment_ = 'ALL'
            
        self.segment_ = file_segment_
        
        if file_segment_ == 'LQD.US':
            file_segment_ = 'IG'
            
        if file_segment_ == 'HYG.US':
            file_segment_= 'HY'


        #self.load_termsheet()
        #self.load_sftp(self.look_back,file_segment_)
        #self.convert_matdate_to_matyear()
      
        self.load_csv_data(self.look_back,file_segment_)
        self.load_marks_local()
        self.convert_matdate_to_matyear()
        self.add_cusip_benchmark_mappping()

        self.ticker_data_mean = self.get_look_back_mean()

        return

    def load_termsheet(self):
        
        url = "https://md-us.tradeweb.com/marks/coridownload/ny?date=20220208&format=md"
        payload={}
        headers = {
            'Authorization': 'Basic c2VjbGlzdGFsbGluZGV4Om1hcmtzMTAwMQ=='
        }
        
        response = requests.request("POST", url, headers=headers, data=payload)
        str_response = response.content.decode('utf-8', 'backslashreplace')
        data = str_response.split('\r\n')
        data.pop(0)
        
        list_termsheets_ = self.parse_marks(data)
        
        df = pd.DataFrame(list_termsheets_)
        df.set_index('CUSIP', inplace = True)
        df.dropna(inplace = True)
        df = df[~df.index.duplicated(keep='first')]

        self.termsheet =df 
        return

    def parse_marks(self,data):
        
        list_termsheets_ = []
        
        for i in data:
            if 'CUSIP' in i:
                vec = i.split('|')
                vec.pop(0)
                vec.pop()
                termsheet_ = {}
                for j in vec:
                    split_ = j.split('=')
                    colname = split_[0]
                    val = split_[1]
                    termsheet_[colname] = val
                
                list_termsheets_.append(termsheet_)


        return list_termsheets_

    def convert_matdate_to_matyear(self):
        
        today_ = datetime.date.today()
        
        self.termsheet['MATDT']= pd.to_datetime(self.termsheet['MATDT'],errors = 'coerce', format ='%Y%m%d' )  
        
        #if out of bounds (i.e. 9999) drop from df. Look into this, may indicate rolling bonds.
        self.termsheet.dropna(inplace = True)
        self.termsheet['MATYRS'] =  self.termsheet['MATDT'].apply(lambda x: relativedelta(x, today_).years)
        
        self.termsheet.dropna(inplace = True)
        mat_years = [-1,0,1,2,3,5,7,10,20,30,50,100]
        self.termsheet['MAT'] = self.termsheet['MATYRS'].apply(lambda x: self.round_maturity_year(x,mat_years))

        return
    
    def round_maturity_year(self,x,mat_years):
        out_ = max([i for i in mat_years if x>=i])
        return out_

    def add_cusip_benchmark_mappping(self):

        mappings_ = pd.read_csv('Benchmarks2021TD.csv')
        mappings_.set_index('BENCHMARK',inplace = True)
        mappings_ = mappings_['BENCHMARK_ABBREV']
        mappings_ = mappings_[~mappings_.index.duplicated(keep='first')]

        #do mapping
        self.termsheet['BENCHMAT'] = self.termsheet['SPRDCUSIP'].map(mappings_)
        return
    
    def load_sftp(self,look_back,segment_):
        
        Hostname = "sfx.tradeweb.com"
        Username = "CORIAIALLINDEX"
        Password = "Zurich@123"
        cnopts = pysftp.CnOpts()
        
        path = '/US_CREDIT/US_CREDIT_Ai-Price/Ai-Price_EOD_Files/' + segment_
        
        cnopts.hostkeys = None 
        with pysftp.Connection(host=Hostname, username=Username, password=Password,cnopts=cnopts) as sftp:
            sftp.cwd(path)
            # Switch to a remote directory
            directory_structure = sftp.listdir_attr()
            
            d_list_ = []
            # Print data
            for attr in directory_structure:
                d_list_.append(re.search(segment_ + '_(.*).csv',attr.filename).group(1))
            
            temp_file_str = directory_structure[0].filename[0:-18]	    
            d_list_.sort(reverse = True)
            d_list_ = d_list_[:look_back]
            self.ticker_data = {}
            
            for d in d_list_:
                file_str = temp_file_str + str(d) +'.csv'
                f = sftp.open(file_str)
                df = pd.read_csv(f)
                df.set_index('CUSIP',inplace = True)
                print()
                if df.empty:
                	pass
                else:
                	self.ticker_data[d] = df
                
            self.dates = list(self.ticker_data.keys())
            
        return
    

    def load_marks_local(self):

        with open('CORI_SecList_20220118.txt',encoding="utf-8",errors = 'backslashreplace') as f:
            lines = f.readlines()

            marks_ = []
            for line in lines:
                marks_.append(line[:-2])

            marks_=marks_[:-5]

        list_termsheets_ = self.parse_marks(marks_)

        df = pd.DataFrame(list_termsheets_)
        df.set_index('CUSIP', inplace = True)
        df.dropna(inplace = True)
        df = df[~df.index.duplicated(keep='first')]
        
        self.termsheet=df

        return

    def load_csv_data(self, look_back,segment_):

        #path = "/home/ec2-user/MarketData/TW_DATA/US_CREDIT_Ai-Price/Ai-Price_EOD_Files/" + segment_
        path = "/Volumes/My Passport/ALLINDEX/TW_DATA/Ai-Price_EOD_Files/" + segment_
        csv_list = glob.glob(os.path.join(path,"*.csv"))
        
        #get templates
        temp_file_str = csv_list[0][0:-18]
        
        d_list_ = []
        
        #select folder
        for f in csv_list:
            d_list_.append(int(re.search(segment_ + '_(.*).csv', f).group(1)))
        
        
        d_list_.sort(reverse = True)
        d_list_ = d_list_[:look_back]
        
        self.ticker_data = {}
        bonds_traded=[]

        for d in d_list_:
            file_str = temp_file_str + str(d) +'.csv'
            df = pd.read_csv(file_str)
            df.set_index('CUSIP',inplace = True)
            
            if df.empty:
                pass
            else:
                self.ticker_data[d] = df
                bonds_traded.append(len(df.index.tolist()))
                
        self.dates = list(self.ticker_data.keys())
        
        try:
            self.average_bonds_traded = sum(bonds_traded)/len(bonds_traded)
        except:
            self.average_bonds_traded = 1
        return
    
    @staticmethod
    def dict_to_df(df_in,var_str_):
    
        first_it = True
  
        for key,data in df_in.items():
            vec_ = data[var_str_]

            if isinstance(vec_, pd.Series):
                vec_.rename(key, inplace = True)
            else:
                vec_=pd.concat({key: vec_}, names=['Date'],axis=1)

            if first_it:
                df_out = vec_
                first_it = False
            else:
                df_out = pd.concat([df_out,vec_], axis = 1)

        df_out.dropna(axis=1, how='all',inplace = True)

        if isinstance(df_out.columns,pd.MultiIndex):
            df_out.columns.set_names('Variable',level=1, inplace = True)    

        return df_out

    def get_look_back_mean(self):
        
        first_it = True

        if len(self.ticker_data.keys()) <=1:
            data = list(self.ticker_data.values())[0]
            df_data = data[ATTRIBUTES_LIST_]
        else:
            first_it = True
            for key,data in self.ticker_data.items():
                if first_it:
                    tmp = data[ATTRIBUTES_LIST_].fillna(0)
                    first_it = False
                else:
                    df_data = data[ATTRIBUTES_LIST_].fillna(0).add(tmp)
                    tmp = df_data
            
            df_data[ATTRIBUTES_LIST_] = df_data[ATTRIBUTES_LIST_].div(len(self.dates))
        
        for i in sorted(self.dates,reverse=True):
            liquidity_ = self.ticker_data[i]['LIQUIDITYSCORE']
            liquidity_=liquidity_.reindex(df_data.index)
            if sum(np.isnan(liquidity_)) > self.average_bonds_traded/2:
                pass
            else:
                df_data['LIQUIDITYSCORE'] = liquidity_
                self.best_data_date = i
               	break
                
        #add termsheet to df
        df_EOD_termsheet = df_data.merge(self.termsheet, left_index = True,right_index = True)
        return  df_EOD_termsheet

    @staticmethod
    def get_benchmark_data(benchmark_str):

        if benchmark_str == 'IG':
            filestr = 'LQD'
        elif benchmark_str == 'HY':
            filestr = 'HYG'
        else:
            filestr = benchmark_str.split('.')[0]

        df = pd.read_csv(filestr + '_holdings.csv')
        benchmark_cons_ = df[['CUSIP','Weight (%)']].copy()
        benchmark_cons_.set_index('CUSIP', inplace = True)
        benchmark_cons_.dropna(inplace = True)

        benchmark_cons_=benchmark_cons_.divide(100.00)
        benchmark_cons_.rename(columns = {'Weight (%)':'Weight'},inplace = True)

        return benchmark_cons_




