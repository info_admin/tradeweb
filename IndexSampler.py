#Stratified sampling function
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.patches as mpl_patches

from sklearn.model_selection import train_test_split
from scipy.stats import iqr
import numpy as np
import math as ma
from scipy.stats import linregress

class IndexSampler(object):
    
    def __init__(self,datahandler,attributes,index_size):
        
        self.datahandler = datahandler
        self.attributes = attributes
        self.index_size = index_size
        
        self.stratified_sample()

        #self.plot_qq()
        #self.plot_hist()
        #self.print_index()
        
    def print_index(self):
        
        print('------------ CustomIndex ------------')
        
        for key,data in self.index_result.items():
            if key == 'Population':
                pass
            else:
                df1 = data[self.attributes]
                print(df1.to_string())

        return
    

    def stratified_sample(self):
    
        cat_str = '__QUANTILE__CAT__'   

        Np = self.datahandler.Constituent_data.shape[0]
        
        index_result = {}
        index_result['Population'] = self.datahandler.Constituent_data
        
        Ns = [self.index_size]
        
        for i in range(len(Ns)):
            sample,rvalue = self.stratified_sampling_process(self.datahandler.Constituent_data,Ns[i],Np,cat_str)
            index_result[Ns[i]] = sample
        
        self.index_result = index_result[self.index_size].index.to_list()
        self.index_rvalue = rvalue
        
        return
        
    
    def stratified_sampling_process(self,data,Ns,Np,cat_str):
       
        #categorise_data
        data_cat = categorise_data(data,cat_str)
        
        #group data
        Sr = Ns/Np
        tmp = data_cat[self.attributes]
        
        tmp = tmp.assign(Size=pd.Series(np.ones(len(data),dtype=int)).values)
        #tmp['Size'] = 1
        tmp_grpd = tmp.groupby(self.attributes).count().reset_index()
        tmp_grpd['Sample size'] = round(Sr * tmp_grpd['Size']).astype(int)
        
        
        distribute_extra_samples(tmp_grpd,Ns)

        print(tmp_grpd)
        
        #iterations of the stratified sampling
        rvalue = 0.0
        out = self.stratified_sampling_iter(data_cat,data,tmp_grpd)
        rvalue = self.statistical_similarity(data,out)
        
        for i in range(0,200):
    
            out_next = self.stratified_sampling_iter(data_cat,data,tmp_grpd)
            rvalue_next = self.statistical_similarity(data,out_next)
            
            if rvalue_next > rvalue:
                out = out_next
                rvalue = rvalue_next
                
            if rvalue > 0.995:
                break

        return out,rvalue
    
    def stratified_sampling_iter(self,data_cat,data,tmp_grpd):
        
        keep_index = True
        first = True
        
         #statify
        for i in range(len(tmp_grpd)):
            # query string
            qry = ''
            for s in range(len(self.attributes)):
                stratum = self.attributes[s]
                value = tmp_grpd.iloc[i][stratum]
                n = tmp_grpd.iloc[i]['Sample size']
                
                if type(value) == str:
                    value = "'" + str(value) + "'"
                
                if s != len(self.attributes)-1:
                    qry = qry + '`' + stratum + '`' + ' == ' + str(value) + ' & '
                else:
                    qry = qry + '`' + stratum + '`' + ' == ' + str(value)
            print(qry)
            if first: 
                stratified_df = data_cat.query(qry).sample(n=n).reset_index(drop=(not keep_index)) #random_state=i
                first = False
            else:
                tmp_df = data_cat.query(qry).sample(n=n).reset_index(drop=(not keep_index))
                stratified_df = pd.concat([stratified_df,tmp_df], ignore_index=True)
       
        
        result = data.loc[stratified_df['CUSIP']]
        
        return result
    
    def statistical_similarity(self,data,data_strat):

        #linear regression for similarity
        interpolation='nearest'
        r_value_list = []
        for s in self.attributes:
            
            #sets are for each iteration to keep things tidy. Caluclate quantiles
            population_set = data[s]
            sample_set = data_strat[s]
            
            #number of quantiles
            quantiles_n = min(len(population_set), len(sample_set))
            
            quantiles = np.linspace(start=0, stop=1, num=int(quantiles_n))
            
            pop_quantiles = np.quantile(population_set, quantiles, interpolation=interpolation)
            sam_quantiles = np.quantile(sample_set, quantiles, interpolation=interpolation)
            
            #do linear regression.
            out = linregress(pop_quantiles,sam_quantiles)
            r_value_list.append(out.rvalue)
    
        return sum(r_value_list)/len(r_value_list)

    
    #plot index corelation
    def plot_qq(self):
    
        plt_col_c = -1
        
        light_blue = '#25bad0' #(37,186,208)
        dark_blue = '#053740' #(5,55,64)
        
        alpha=0.7
        
        interpolation='nearest'
        
        fig, ax =  plt.subplots(len(self.index_result)-1, len(self.attributes))
        
        for sample_key in self.index_result:
            
            if sample_key == 'Population':
                pass
            else: 
                plt_col_c = plt_col_c + 1
                 
            plt_row_c = -1
            for s in self.attributes:    
                plt_row_c = plt_row_c + 1 
                if sample_key == 'Population':
                    
                    pass
                    
                else:
                   
                    #sets are for each iteration to keep things tidy. Caluclate quantiles
                    population_set = self.index_result['Population'][s]
                    sample_set = self.index_result[sample_key][s]
                    
                    #number of quantiles
                    quantiles_n = min(len(population_set), len(sample_set))
                    
                    quantiles = np.linspace(start=0, stop=1, num=int(quantiles_n))
                    
                    pop_quantiles = np.quantile(population_set, quantiles, interpolation=interpolation)
                    sam_quantiles = np.quantile(sample_set, quantiles, interpolation=interpolation)
                    
                    #regression
                    res = linregress(pop_quantiles,sam_quantiles)
                    corr_val = res.rvalue
                    
                    
                    if len(self.index_result)-1 == 1:
                    
                        # Quantile-quantile plot
                        ax[plt_row_c].scatter(np.sort(pop_quantiles), np.sort(sam_quantiles), c=dark_blue, alpha = alpha)
                        ax[plt_row_c].plot(np.sort(pop_quantiles), res.intercept + res.slope*np.sort(pop_quantiles), 'r', label='fitted line', c=light_blue, alpha = alpha)
                        
                        
                        if s == 'Cumulative Daily Return':
                            name_str = s
                        else:
                            name_str = 'Average ' + s
                        
                        get_stats_legend(round(corr_val,4),s,ax,plt_row_c)
                        ax[plt_row_c].set(xlabel= 'Population', ylabel= 'Sample')
               
                    else: 
                        
                
                        
                        # Quantile-quantile plot
                        ax[plt_col_c,plt_row_c].scatter(np.sort(pop_quantiles), np.sort(sam_quantiles), c=dark_blue, alpha = alpha)
                        ax[plt_col_c,plt_row_c].plot(np.sort(pop_quantiles), res.intercept + res.slope*np.sort(pop_quantiles), 'r', label='fitted line', c=light_blue, alpha = alpha)
                        
                        if s == 'Cumulative Daily Return':
                            name_str = s
                        else:
                            name_str = 'Average ' + s
                        
                        get_stats_legend(round(corr_val,4),name_str,ax,plt_col_c,plt_row_c)
                        
                        ax[plt_col_c,plt_row_c].set(xlabel= 'Population', ylabel= 'Sample')
                        
        return
    
    
    def plot_hist(self):
        """
        plot_hist: Plots both population vs sample as a histogam and calculates there similarity (Chi-Square) 
        for each strata. The number of bins for the population are caculated using a Freedam-diaconis metric and for each strata
         and it applied to the sample groups for direct comparison.  
        """
        plt_col_c = -1
        
        sample_colour = '#25bad0' #(37,186,208)
        pop_colour = '#053740' #(5,55,64)
        
        fig, ax =  plt.subplots(len(self.index_result)-1, len(self.attributes))
        
        for sample_key in self.index_result:
            
            if sample_key == 'Population':
                pass
            else: 
                plt_col_c = plt_col_c + 1
                 
            plt_row_c = -1
            for s in self.attributes:    
                plt_row_c = plt_row_c + 1 
                if sample_key == 'Population':
                    
                    pass
                    
                else:
                    #set histgoram paramets and calculate bins based on freedam-diaconis metric on population data.
                    #sets are for each iteration to keep things tidy
                    population_set = self.index_result['Population'][s]
                    sample_set = self.index_result[sample_key][s]
                    
                    #bin calculation
                    bin_width,bin_no = freedman_diaconis(sample_set)
                    
                    kwargs = dict(histtype='stepfilled', alpha=0.7, density=True, bins = bin_no*2)
                    
                    if len(self.index_result)-1 == 1:
                        n_p, bins_p, patches_p = ax[plt_row_c].hist(population_set, color = pop_colour, ec = pop_colour, **kwargs)
                        n_s, bins_s, patches_s = ax[plt_row_c].hist(sample_set, color = sample_colour, ec = sample_colour, **kwargs)
                        
            
                        ax[plt_row_c].legend([self.datahandler.index_focus.split('.')[0] + ' Index','Sample Index'])
                        
                        if s == 'Cumulative Daily Return':
                            ax[plt_row_c].set(xlabel=s, ylabel="Probability density")
                            
                        else:
                            ax[plt_row_c].set(xlabel='Average ' + s, ylabel="Probability density")
               
                    else: 
                        n_p, bins_p, patches_p = ax[plt_col_c,plt_row_c].hist(population_set, color = pop_colour, ec = pop_colour, **kwargs)
                        n_s, bins_s, patches_s = ax[plt_col_c,plt_row_c].hist(sample_set, color = sample_colour, ec = sample_colour, **kwargs)
                        
                        ax[plt_col_c,plt_row_c].legend([self.datahandler.index_focus.split('.')[0] + ' Index','Sample ($n$ = ' + str(sample_key) + ')'])
                        
    
                        if s == 'Cumulative Daily Return':
                            ax[plt_col_c,plt_row_c].set(xlabel=s, ylabel="Probability density")
                        else:
                            ax[plt_col_c,plt_row_c].set(xlabel='Average ' + s, ylabel="Probability density")      
                    
        plt.show()         

        return

    
   

#plotting functions
def freedman_diaconis(data):
    """
    Use Freedman Diaconis rule to compute optimal histogram bin width and size. 
    Returns both "width" or "bins".
    """

    data = np.asarray(data, dtype=np.float_)
    IQR  = iqr(data, rng=(25, 75), scale="raw", nan_policy="omit")
    N    = data.size
    bw   = (2 * IQR) / np.power(N, 1/3)


    width = bw
    datmin, datmax = data.min(), data.max()
    datrng = datmax - datmin
    bin_size = int((datrng / bw) + 1)
    return width, bin_size

def get_stats_legend(corr_val,attribute_str,ax,*args):
    
    #create a list with two empty handles (or more if needed)
    handles = [mpl_patches.Rectangle((0, 0), 1, 1, fc="white", ec="white", 
                                     lw=0, alpha=0)] * 2
    
    # create the corresponding number of labels (= the text you want to display)
    labels = []
    labels.append(attribute_str)
    labels.append("$R$ = {}".format(corr_val))
    
    # create the legend, supressing the blank space of the empty line symbol and the
    # padding between symbol and label by setting handlelenght and handletextpad
    
    if len(args) == 1:
        return ax[args].legend(handles, labels, loc='best', fontsize='small', 
                  fancybox=True, framealpha=0.7, 
                  handlelength=0, handletextpad=0)
    else:
        return  ax[args[0],args[1]].legend(handles, labels, loc='best', fontsize='small', 
          fancybox=True, framealpha=0.7, 
          handlelength=0, handletextpad=0)
    
    return

def plot_return(Index_obj,returns):
    
    legend_vec = []
    
    plt.figure() 
    
    for k in returns:
        data = returns[k]
        cumulative_return = (data + 1).cumprod()
        plt.plot(cumulative_return)
        plt.show()
        
        if k == 'Population':
            legend_vec.append(Index_obj.Code.split('.')[0] + ' Index')
        else: 
            legend_vec.append('Sample ($n$ = ' + str(k) + ')')
   
    '''    
    weight_check = Index_obj.Index_EOD_data['FTSE.INDX']['Adjusted close'].pct_change()
    weight_check = (weight_check + 1).cumprod()
        
    plt.plot(weight_check, color = 'tab:pink', linestyle = '--')
    '''
    
    plt.legend(legend_vec)    
    plt.grid()
    plt.show()
    
    return




#utility functions
def round_decimals_down(number:float, decimals:int=2):
    """
    Returns a value rounded down to a specific number of decimal places.
    """
    if not isinstance(decimals, int):
        raise TypeError("decimal places must be an integer")
    elif decimals < 0:
        raise ValueError("decimal places has to be 0 or more")
    elif decimals == 0:
        return ma.floor(number)

    factor = 10 ** decimals
    return ma.floor(number * factor) / factor


def distribute_extra_samples(tmp_grpd,Ns):
        
        #make sure the number of sample is identical
        group_count = sum(tmp_grpd['Sample size'])
        count_diff = Ns - group_count
        
        if count_diff != 0:
            #sort values
            tmp_df = tmp_grpd.sort_values('Size', ascending = False)
            
            i = -1
            
            while count_diff > 0:
                
                i = i + 1
                idx = tmp_df.index[i]
                
                samp = tmp_grpd['Sample size'].loc[idx]
                pop = tmp_grpd['Size'].loc[idx]
                
                cur_diff = pop - samp
                
                if cur_diff <= count_diff:
                    
                    samp = samp + cur_diff      
                    
                    count_diff = count_diff - cur_diff
                
                elif cur_diff > count_diff:
                   
                    samp = samp + count_diff 
                    break
                    
                tmp_grpd['Sample size'].loc[idx] = samp
        else: 
            pass
            
def categorise_data(data, op_str):
    
        data_cat = data.copy()
        
        if op_str == '__QUANTILE__CAT__':
            
            quarts_arr = [0.25,0.5,0.75]
            
            for col in data:
                column = data[col]
                quarts = np.quantile(column,quarts_arr)
                data_cat[col]=data_cat[col].apply(quartile_classification, args = [quarts])
                 
            
        elif op_str == '__DISCRETE__CAT__':
            
            n_points = 5
            arr = np.linspace(0,1,n_points,endpoint = False)
            quarts_arr = np.delete(arr,0)
            
            for col in data:
                column = data[col]
                quarts = np.quantile(column,quarts_arr)
                data_cat[col]=data_cat[col].apply(disc_classification, args = [quarts])            
            
        else:
            raise ValueError ('ALLINDEX: Categorization method not recognised.')
    
                
        return data_cat
    

def disc_classification(value,disc_points):
    
    count = 0
    for i in range(len(disc_points)):
        count = count + 1
        
        if i == 0:
            
            if value < disc_points[i]:
                return 'D' + str(count)
            
        elif 0 < i < len(disc_points)-1:
            
            if  disc_points[i-1]  <= value < disc_points[i]:
                return 'D' + str(count)
            
        elif i == len(disc_points)-1:
            
            if disc_points[i-1] <= value < disc_points[i]:
                return 'D' + str(count)
            
            elif value >= disc_points[i]:
                return 'D' + str(count + 1)
            else:
                return 'Not working'
            
        else: 
            raise ValueError ('ALLINDEX: Discretisation not computed')
    
    return     
 

def quartile_classification(value,quarts):
    
        if value < quarts[0]:
            return 'Q1'
        elif quarts[0]  <= value < quarts[2]:
            return 'Q2'
        elif value >= quarts[2]:
            return 'Q3'
        else:
            raise ValueError ('ALLINDEX: Quartile classification error')           
        return

