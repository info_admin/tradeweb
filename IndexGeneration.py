
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 16 11:30:24 2021

@author: Benjamin Robinson (Quant. Dev) - br@allindex.com
"""
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=RuntimeWarning)
import pandas as pd
import numpy as np
import math as ma
from IndexStats import IndexStats
from scipy.stats import linregress
from DataHandler import DataHandler
from Constraints import CONSTRANT_LIST_

from GenAl.Optimizer import TrainOptimizer
from GenAl.Asset import Asset

import matplotlib.pyplot as plt

class Index(object):
    '''
    Index baseclass
    '''
    @staticmethod
    def constrain_df(attributes_,data_EOD_termsheet):

        #only allow constraining attributes (if they exist).
        inter_ = list(set(CONSTRANT_LIST_).intersection(list(attributes_.keys())))
        df_out = data_EOD_termsheet

        if inter_:
            qry_str = ''
            for att in inter_:

                inc_ = []
                exc_ = []

                for att_item in attributes_[att]:

                    if '-' in att_item:
                        exc_.append(att_item.replace('-',''))
                        operator_str_ = '~'

                    elif '+' in att_item:
                        inc_.append(att_item.replace('+',''))
                        operator_str_ = ''

                    else:
                        inc_.append(att_item)
                        operator_str_ = ''

                if exc_: 
                    qry_str  = qry_str +'(' + operator_str_ + 'df_out[' +"'" + att + "'" + ']' + '.isin([' +  ', '.join(f"'{x}'" for x in exc_) + "]))" + ' & '
                if inc_:
                    qry_str  = qry_str +'(' + operator_str_ + 'df_out[' +"'" + att + "'" + ']' + '.isin([' +  ', '.join(f"'{x}'" for x in inc_) + "]))" + ' & '
        
                
            qry_str = qry_str[:-3]
            qry_str = 'df_out[' + qry_str + ']'
            df_out = eval(qry_str)

        return df_out

    def rank_index_sampling(self,df_data):

        df_rank_ = pd.DataFrame(index = df_data.index, columns = [*self.attributes.keys()])
        #switch ranking based on based on High or Low attribute relationship, and Multiply by weights
        for att,params in self.attributes.items():
            if att not in CONSTRANT_LIST_:
                if params[1] == 'High':
                    df_rank_[att] = (df_data[att].rank(ascending=False))*params[0]
                elif params[1] == 'Low':
                    df_rank_[att] = (df_data[att].rank())*params[0]
                elif params[1] is not str:
                    diff_ = abs(df_data[att]-params[1])
                    df_rank_[att]=(diff_.rank())*params[0] 
                else:
                    raise ValueError('ALLINDEX User Error: Please select high or low or target.')

        return df_rank_

    def sector_concentration_limits(self,df_data,df_ranked_):
     
        df_ranked_ = df_ranked_.sum(axis = 1)   
        df_ranked_=df_ranked_.to_frame()
        df_ranked_ = pd.concat([df_ranked_,df_data['SECTOR']],axis = 1)
        df_ranked_.rename(columns={0:'Ranked'},inplace=True)
        
        basket = df_ranked_['Ranked'].nsmallest(n = self.sampling_size)
        Max_Per_Sector = int(ma.ceil(self.index_size*self.max_sector_concentration))

        overflowed_sectors=[]
        for i in range(0,100):
            #limit sector concentrations.
            df_rank_sector = pd.merge(basket.to_frame(),df_data['SECTOR'].to_frame(),left_index=True,right_index=True)
            df_rank_sector_grpd  =  df_rank_sector.groupby('SECTOR').count()
            
            new_overflowed_sectors = df_rank_sector_grpd.index[df_rank_sector_grpd['Ranked'] > Max_Per_Sector].tolist()
            overflowed_sectors = overflowed_sectors+new_overflowed_sectors
            if new_overflowed_sectors:
                basket = self.sector_iterations_(df_ranked_,basket,Max_Per_Sector,overflowed_sectors,df_rank_sector)
            else:
                break

        return basket.index.tolist()
    
    def sector_iterations_(self,df_ranked_,basket,Max_Per_Sector,overflowed_sectors,df_rank_sector):

        DROP_LIST = []

        for sec in overflowed_sectors:
            sector_ = df_rank_sector[df_rank_sector['SECTOR']==sec]
            N_drop = sector_.index.size-Max_Per_Sector
            DROP_LIST = DROP_LIST + (sector_['Ranked'].nlargest(n=N_drop)).index.tolist()
        

        N_new=len(DROP_LIST)
        df_ranked_ = df_ranked_[((~df_ranked_['SECTOR'].isin(overflowed_sectors)) & (~df_ranked_.index.isin(basket.index)))]
        df_new_bonds_ = df_ranked_['Ranked'].nsmallest(n =N_new)

        new_basket = df_new_bonds_.append(basket.drop(index = DROP_LIST))

        return new_basket

    def beta_optimiser_func_(self,data_ranked_,sample_size=None):
  
        N = self.index_size

        df_data_=data_ranked_.sum(axis=1).nsmallest(int(N*1.5))

        if len(df_data_.index) <= N:
            return df_data_.index.tolist()

        BENCH_WEIGHT = DataHandler.get_benchmark_data(self.datahandler.segment_)

        df_price_data = DataHandler.dict_to_df(self.datahandler_stats.ticker_data,'MID')
        df_benchmark_return = df_price_data.pct_change(axis = 'columns')
        df_benchmark_return = pd.merge(df_benchmark_return,BENCH_WEIGHT,left_index = True, right_index = True)
        df_benchmark_return = df_benchmark_return[list(df_benchmark_return.columns)].multiply(df_benchmark_return['Weight'], axis='index')
        df_benchmark_return.drop(['Weight'],axis = 1, inplace= True)
        df_benchmark_return = df_benchmark_return.sum()

        beta_diff=1000000.0

        for i in range(0,N*N):

            INDEX_LIST = (df_data_.sample(n=N)).index.tolist()
            INDEX_WEIGHT = 1 / len(INDEX_LIST) 

            df_index_data = df_price_data.loc[INDEX_LIST]
            df_index_return = df_index_data.pct_change(axis = 'columns')
            df_index_return = df_index_return.multiply(INDEX_WEIGHT).sum()

            df_all_return_ = pd.merge(df_index_return.to_frame(),df_benchmark_return.to_frame(),left_index=True,right_index=True)
            df_all_return_.rename(columns={'0_x':'CustomIndex','0_y':'BenchmarkIndex'},inplace=True)
            df_all_return_ = df_all_return_[~df_all_return_.isin([np.nan, np.inf, -np.inf]).any(1)]

            #beta
            x = np.array(df_all_return_['CustomIndex'])
            y = np.array(df_all_return_['BenchmarkIndex'])
            beta_out = linregress(x,y)
            beta_diff_new = abs(1.0-beta_out.slope)
            
            if beta_diff_new < beta_diff:
                beta_diff = beta_diff_new
                FINAL_INDEX_LIST = INDEX_LIST

            #print('Beta variation:', abs(beta_diff))
            #print('Beta value: ',beta_diff)

        CUSIP_DIFF = list(set(FINAL_INDEX_LIST).intersection(df_data_.index.tolist()))
        CUSIPS_ALT = df_data_.reindex(CUSIP_DIFF).nsmallest(int(N*0.4)).index.tolist()

        return FINAL_INDEX_LIST + CUSIPS_ALT


    def GA_function(self,data_ranked_):

        N = self.index_size

        df_data_=data_ranked_.sum(axis=1).nsmallest(int(N*2))

        if len(df_data_.index) <= N:
            return df_data_.index.tolist()

        df_price_data = DataHandler.dict_to_df(self.datahandler_stats.ticker_data,'MID')

        BENCH_WEIGHT = DataHandler.get_benchmark_data(self.datahandler.segment_)
        BENCH_WEIGHT = BENCH_WEIGHT[BENCH_WEIGHT.index.isin(df_price_data.index)]
        BENCH_LIST = BENCH_WEIGHT.index.tolist()

        INDEX_LIST = df_data_.index.tolist()

        df_assets=IndexStats.calculate_TR(df_price_data,self.datahandler.termsheet['CPN'],1000.0,cusip_list=INDEX_LIST,weight=None)
        df_bench=IndexStats.calculate_TR(df_price_data,self.datahandler.termsheet['CPN'],1000.0,cusip_list=BENCH_LIST,weight=BENCH_WEIGHT)

        df_assets=df_assets.T
        assets = {}
        count = -1
        for column in df_assets:
            count = count+1
            data = df_assets[column].reset_index().rename(columns={'index':'Date'})
            data.rename(columns={column:'Price'},inplace=True)
            assets[count] = Asset(column,data)
            print(assets[count].prices)

        df_bench=df_bench.sum()
        df_bench = df_bench.reset_index()
        df_bench.rename(columns={'index':'Date',0:'Price'},inplace=True)
        index = Asset('Benchmark',df_bench)
        print(index.prices)
        
        print('Index size:',N)
        print('Number of stocks:', len(assets))

        tracking_params = {
            'minWeight':0.01,
            'transactionCost': 1.75, 
            'lambda': 0.99,
            'predictionInterval':100,
            'rebalancingPeriod':30
        }
        
        genetic_params = {
            'probCrossover': 0.8,
            'probReplacement': 0.6,
            'probMutation': 0.075,
            'sizeTournament': 4
            }

        opt = TrainOptimizer(index, assets)
        opt.train(100,N,100,genetic_params,tracking_params)
        best = opt.best_portfolio()

        print('Total weight:',best[1].sum())
        weights = dict(zip(best[0], best[1].tolist()))
        print(weights)

        alternative_ = df_price_data.T.sample(n=N).index.tolist()

        return [*weights] + alternative_

    
class ThemeBasedIndex(Index):
    
    def __init__(self, datahandler,datahandler_stats,attributes,look_back, index_size,max_sector_concentration,beta_optimiser,replace_list):
        
        self.attributes = attributes
        self.index_size = index_size
        self.max_sector_concentration = max_sector_concentration
        self.beta_optimiser=beta_optimiser
        self.replace_list = replace_list

        self.datahandler = datahandler
        self.datahandler_stats = datahandler_stats
        
        #apply index constraints
        self.ticker_data_mean_constrained = Index.constrain_df(self.attributes,self.datahandler.ticker_data_mean)

        if self.ticker_data_mean_constrained.empty:
            print('No bonds available, adapt constraints.')
            exit()
        
        #create index
        self.generate_index()
        
    def generate_index(self): 
        
        #This function is for the selection process
        #Get average looking back
        df_data = self.ticker_data_mean_constrained[[*self.attributes]].copy()
        df_data = df_data.dropna(subset=[*self.attributes])

        #remove replacement cusips.
        for i in self.replace_list:
            try:
                df_data=df_data.drop(i)
            except:
                pass
        
        df_rank_ = self.rank_index_sampling(df_data)

        if df_rank_.empty:print('No bonds available, adapt constraints.');exit()

        self.sampling_size = int(self.index_size*1.4)

        if self.beta_optimiser:
                output_smallest_=self.beta_optimiser_func_(df_rank_)
        elif self.max_sector_concentration is not None:
            try:
                output_smallest_ = self.sector_concentration_limits(df_data,df_rank_)
            except:
                print('Limited bonds available for concentration limits')
                output_smallest_ = (df_rank_.sum(axis=1).nsmallest(n = self.sampling_size)).index.tolist()     
        else:
                output_smallest_ = (df_rank_.sum(axis=1).nsmallest(n = self.sampling_size)).index.tolist()

   
        self.index_result = {}
        self.index_result['IndexCon'] = output_smallest_[0:self.index_size]
        self.index_result['IndexSug'] = output_smallest_[self.index_size:]
        self.df_beta = None
        return
    
class ETFBasedIndex(Index):
    
    def __init__(self, datahandler,datahandler_stats,attributes,look_back, index_size,max_sector_concentration,beta_optimiser,replace_list):

        self.attributes = attributes
        self.index_size = index_size
        self.max_sector_concentration = max_sector_concentration
        self.beta_optimiser=beta_optimiser
        self.replace_list = replace_list
        
        self.datahandler = datahandler
        self.datahandler_stats = datahandler_stats
        
        sampling_op = 'RANK'

        if self.attributes['MKTSEG'][0] == 'LQD.US':
            self.attributes['MKTSEG'][0] = 'IG'
        elif self.attributes['MKTSEG'][0] == 'HYG.US':
            self.attributes['MKTSEG'][0] = 'HY'
        
        #apply index constraints
        self.ticker_data_mean_constrained = Index.constrain_df(self.attributes,self.datahandler.ticker_data_mean)
        
        if self.ticker_data_mean_constrained.empty:
            print('No bonds available, adapt constraints.')
            exit()

        self.generate_index(sampling_op)
        
    
    def generate_index(self,sampling_op):
        
        #merge dfs. add a beta columns and a column to indicate constituent of etf.
        df_beta = self.calculate_beta()
        df_data = self.ticker_data_mean_constrained.loc[self.ticker_data_mean_constrained.index.intersection(list(df_beta.index))].copy() 

        df_data = df_data.dropna(subset=[*self.attributes])
  
        #remove replacement cusips.
        for i in self.replace_list:
            try:
                df_data=df_data.drop(i)
            except:
                pass

        #ranking
        df_rank_ = self.rank_index_sampling(df_data)

        if df_rank_.empty:print('No bonds available, adapt constraints.');exit()

        self.sampling_size = int(self.index_size*1.4)

        if self.beta_optimiser:
            #output_smallest_=self.beta_optimiser_func_(df_rank_)
            output_smallest_ = self.GA_function(df_rank_)
        elif self.max_sector_concentration is not None:
            try:
            	output_smallest_ = self.sector_concentration_limits(df_data,df_rank_)
            except:
                print('Limited bonds available for concentration limits')
                output_smallest_ = (df_rank_.sum(axis=1).nsmallest(n = self.sampling_size)).index.tolist()     
        else:
            output_smallest_ = (df_rank_.sum(axis=1).nsmallest(n = self.sampling_size)).index.tolist()
   
        
        self.index_result = {}
        self.index_result['IndexCon'] = output_smallest_[0:self.index_size]
        self.index_result['IndexSug'] = output_smallest_[self.index_size:]
        self.df_beta = df_beta
    
        return
    
    def calculate_beta(self):

        #compute weighted etf index.
        #get constieunts from ishares (not historical) .csv
        constituents_ =  DataHandler.get_benchmark_data(self.datahandler.segment_)
    
        #only allow constituents that exist in 
        intersec_ = self.ticker_data_mean_constrained.index.intersection(constituents_.index)
        constituents_ = constituents_.loc[intersec_].copy()

        #create datahandler
        lookback = 100
        DhObj = DataHandler(self.attributes,lookback)
        
        df_beta = self.get_beta_(DhObj.ticker_data,constituents_,lookback)
        beta_list = df_beta.index.difference(constituents_.index)
     
        df_beta = df_beta.loc[beta_list]

        #get beta's between 0 and 2
        df_beta = df_beta[df_beta['BETA'].between(0.75,1.25)]
        #out_list = list(constituents_.index) + list(df_beta.index)

        df_beta.rename(columns = {'BETA':'CONS/BETA'},inplace=True)
        df_con_beta = pd.DataFrame(['Con']*len(constituents_.index.tolist()),columns = ['CONS/BETA'],index = constituents_.index)
        df_beta = df_beta.append(df_con_beta)
   
        return df_beta
    
    
    def get_beta_(self,ticker_data,constituents_,lookback):

        df_data = DataHandler.dict_to_df(ticker_data,'MID')
        df_data.replace([np.inf, -np.inf], np.nan, inplace=True)
        df_data.interpolate(axis = 1)

        df_data = df_data.pct_change(axis='columns')
        df_data.dropna(axis=1, how='all',inplace = True)
        
        #merge constituents
        df_etf_ = pd.merge(df_data,constituents_,left_index = True, right_index = True)
        df_etf_.replace([np.inf, -np.inf], np.nan, inplace=True)
        df_etf_.interpolate()
        df_etf_ = df_etf_[list(df_data.columns)].multiply(df_etf_['Weight'], axis="index").sum()

        y = np.array(df_etf_) #etf index

        beta_ = {}
        #iterate through bonds and calculate beta.
        for index, row in df_data.iterrows():
            x = np.array(row) #bond 
            try:
                out = linregress(x,y)
            except ValueError:
                pass
            else:
                beta_[index] = out.slope
        
        df_out_ = pd.DataFrame.from_dict(beta_, orient='index')
        df_out_.rename(columns = {0:'BETA'},inplace = True)

        return df_out_

