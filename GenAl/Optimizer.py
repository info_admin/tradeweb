
from this import d
from GenAl.GA import GeneticAlgorithm
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import linregress
import matplotlib.patches as mpl_patches


def get_genetics_parameters():
    probCrossover, probReplacement, probMutation, sizeTournament = 0.8, 0.6, 0.075, 4
    return probCrossover, probReplacement, probMutation, sizeTournament

def get_tracker_parameters():
    minWeight, transactionCost, lmbda, predictionInterval, rebalancingPeriod = 0.01, 1.75, 0.6, 100, 30
    return minWeight, transactionCost, lmbda, predictionInterval, rebalancingPeriod

class TrainOptimizer:
    def __init__(self,index,stocks,equal=False):
        '''
        Args:
            index (Stock)
                Index (FTSE-100) represented as a Stock
            stocks (List[Stock])
                List of stocks containing the assets in the portfolio
        '''
        # pdb.set_trace()
        self.index = index
        self.stocks = stocks
        self.equal = equal
        
        self.max_length=len(self.stocks[0].prices)

        self.train_interval = [0,int(0.90*self.max_length)]
        self.validate_interval = [self.train_interval[1],int(0.95*self.max_length)]
        self.test_interval = [self.validate_interval[1],int(self.max_length)-1]

    def train(self, ga_cycles, portfolio_size, population_size,genetic_params, tracker_params):
        '''
        Args:
            train_interval (list[int]) # E.G. (10, 100)
                integers - specifying the range (STAR, END) of data we want to use to train the Optimizer 
        '''

        # STEPS:
        # 1. Intialize GA parameters
        # 2. Set Training/Prediction intervals    
        # 3. Initialize Evaluator 
        # 4. Initialize Optimizer
        # 5. Train
        
        # pdb.set_trace()
        minWeight, transactionCost, lmbda, predictionInterval, rebalancingPeriod = get_tracker_parameters() # we set these paramaters as per the PFGA applciaiton
        probCrossover, probReplacement, probMutation, sizeTournament = get_genetics_parameters()

        self.optimizer = GeneticAlgorithm(
                    ga_cycles, portfolio_size,
                    self.stocks, 
                    self.index,
                    self.equal,
                    genetic_params=(genetic_params['probCrossover'], genetic_params['probReplacement'], genetic_params['probMutation'], genetic_params['sizeTournament']), 
                    tracker_params=(portfolio_size, tracker_params['minWeight'], tracker_params['rebalancingPeriod'], tracker_params['transactionCost'], tracker_params['lambda'])
                )

        self.optimizer.run(self.train_interval, self.validate_interval, self.test_interval)
        self.print_params(genetic_params,tracker_params)
        self.plot_tracking()
        self.plot_iteration_stats()
    
    def best_portfolio(self, datatype='test'):
        return self.optimizer.best_portfolio(datatype)

    def iteration_stats(self):
        return self.optimizer.iteration_stats

    def solution(self):
        return self.optimizer.solution()

    def print_params(self,genetic_params,tracker_params):

        print('----- Genetic Parameters -----')
        print(pd.DataFrame.from_dict(genetic_params,orient = 'index'))

        print('----- Tracker Params -----')
        print(pd.DataFrame.from_dict(tracker_params,orient = 'index'))

        best_portfolio = self.best_portfolio()
        best_portfolio = dict(zip(best_portfolio[0], best_portfolio[1].tolist()))
        print('----- Best Portfolio -----')
        print(pd.DataFrame.from_dict(best_portfolio,orient = 'index'))

        return

    def plot_iteration_stats(self):

        stats = self.iteration_stats()

        plt.figure()
        plt.plot(stats['MaxFitness'])
        plt.plot(stats['MeanFitness'])
        plt.xlabel('Generation Pool (iterations)')
        plt.ylabel('Fitness')
        plt.legend(['Max Fitness','Mean Fitness'])
        plt.grid()
        plt.show()

        return

    def plot_tracking(self):

        light_blue = '#25bad0' #(37,186,208)
        dark_blue = '#053740' #(5,55,64)
        alpha=0.7

        best_portfolio = self.best_portfolio(datatype='test')
        asset = best_portfolio[3]

        portfolio_price = [0.0 for i in range(self.max_length)] 

        for k in range(len(asset)):
            prices = list(self.stocks[int(asset[k])].prices.values())
            try:
                for p in range(len(portfolio_price)):
                    portfolio_price[p] = portfolio_price[p] + (prices[p] * best_portfolio[1][k])
            except:
                pass

        index_price = np.array(list(self.index.prices.values()))
        index_dates = np.array([*self.index.dates.values()])

        portfolio_dates = [*self.stocks[0].dates.values()]

        df_portfolio = pd.DataFrame({'Date':portfolio_dates,'Portfolio Price': portfolio_price}).set_index('Date')
        df_index = pd.DataFrame({'Date':index_dates,'Index Price':index_price}).set_index('Date')

        df_ndata = pd.merge(df_portfolio,df_index,left_index = True,right_index=True)

        df_ndata = (df_ndata - df_ndata.mean()) / (df_ndata.max() - df_ndata.min())
        df_ndata+=100

        #plot tracking
        plt.figure()
        plt.plot(df_ndata['Portfolio Price'],alpha=alpha)
        plt.plot(df_ndata['Index Price'],alpha=alpha)
        plt.legend(['Portfolio','Index'])
        plt.ylabel('Price')
        plt.xlabel('Date')
        plt.grid()
        plt.show()
                    
        #number of quantiles
        quantiles_n = min(len(df_ndata['Index Price']), len(df_ndata['Portfolio Price']))        
        quantiles = np.linspace(start=0, stop=1, num=int(quantiles_n))
        index_quantiles = np.quantile(df_ndata['Index Price'], quantiles, interpolation='nearest')
        port_quantiles = np.quantile(df_ndata['Portfolio Price'], quantiles, interpolation='nearest')
                    
        #regression
        res = linregress(index_quantiles,port_quantiles)
        corr_val = res.rvalue

        fig, ax = plt.subplots()
        ax.scatter(np.sort(index_quantiles), np.sort(port_quantiles), c=dark_blue, alpha = alpha)
        ax.plot(np.sort(index_quantiles), res.intercept + res.slope*np.sort(index_quantiles), 'r', label='fitted line', c=light_blue, alpha = alpha)
        #get_stats_legend(round(corr_val,4),ax,1)
        ax.set(xlabel= 'Index', ylabel= 'Portfolio')
        ax.grid()
        plt.show()

        print("---- Stats ----")
        print('Correlation: ',corr_val)
        
        return

def get_stats_legend(corr_val,ax,*args):
    
    #create a list with two empty handles (or more if needed)
    handles = [mpl_patches.Rectangle((0, 0), 1, 1, fc="white", ec="white", 
                                     lw=0, alpha=0)] * 2
    
    # create the corresponding number of labels (= the text you want to display)
    labels = []
    #labels.append(attribute_str)
    labels.append("$R$ = {}".format(corr_val))
    #labels.append("$N$ = {}".format(N))
    
    # create the legend, supressing the blank space of the empty line symbol and the
    # padding between symbol and label by setting handlelenght and handletextpad
    
    if len(args) == 1:
        return ax.legend(handles, labels, loc='best', fontsize='small', 
                  fancybox=True, framealpha=0.7, 
                  handlelength=0, handletextpad=0)
    else:
        return  ax.legend(handles, labels, loc='best', fontsize='small', 
          fancybox=True, framealpha=0.7, 
          handlelength=0, handletextpad=0)
    
    return


