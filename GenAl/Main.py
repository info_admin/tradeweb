import glob
import pandas
import pdb
import numpy

#from Optimizer import TrainOptimizer
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error

stockfiles = glob.glob('Data/Stocks/*.csv')
indexfile = 'Data/FTSE/FTSE-100 Index.csv'
stocks = {}
epics = {}

for index, filename in enumerate(stockfiles):
    epic = filename.split('/')[-1].split('.')[0]
    data = pandas.read_csv(filename, names=['Date', 'ClosingPrice'], skiprows=[0])
    #stock = Stock(epic, data)

    stocks[index] = data
    epics[index] = epic
    print(index)


print(stocks)
indexepic = indexfile.split('/')[1].split('.')[0].split(' ')[0]
data = pandas.read_csv(indexfile, usecols=['Date', 'Adj Close'])
data.columns = ['Date', 'ClosingPrice']
index = Stock(indexepic, data)

print(stock.prices)

opt = TrainOptimizer(index, stocks)
opt.train(train_interval=[0, 2500], validate_interval=[2500, 2700], test_interval=[2700, 2800])
best = opt.best_portfolio()


weights = dict(zip(best[0], best[1]))


data = map(lambda X: (index.dates[X], index.prices[X]), index.prices)
data = pandas.DataFrame(data)
data.index = data[0]
data.index = pandas.to_datetime(data.index)
data.drop(0, axis=1, inplace=True)
data.columns = ['FTSE']

for stock in stocks.values():
    d = map(lambda X: (stock.dates[X], stock.prices[X]), stock.prices)
    d = pandas.DataFrame(d)
    d.index = d[0]
    d.index = pandas.to_datetime(d.index)
    d.drop(0, axis=1, inplace=True)
    
    names = data.columns.tolist()
    names.append(stock.epic)
    data = pandas.concat([data, d], ignore_index=True, axis=1)
    data.columns = names


data.index.name = 'Date'
data.dropna(axis=0, thresh=5, inplace=True)
data.to_csv('Data/COLLECT.csv')

fdata = data['2015-01-02':]

print(weights)

weight_vector = map(lambda X: weights[X], sorted(weights))
fdata['SyFTSE'] = numpy.dot(fdata[fdata.columns[1:]].sort(axis=1), weight_vector)
fdata.tail()

# data = data.cumsum()
plt.figure()
fdata.FTSE.plot(figsize=(10, 10))
fdata.SyFTSE.plot(figsize=(10, 10))
plt.legend(loc='best')



